#!/bin/bash

echo "apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: ${NAME_PREFIX}-petclinic
  name: ${NAME_PREFIX}-petclinic
spec:
  replicas: 1
  selector:
    matchLabels:
      run: ${NAME_PREFIX}-petclinic
  template:
    metadata:
      labels:
        run: ${NAME_PREFIX}-petclinic
    spec:
      containers:
      - name: ${NAME_PREFIX}-petclinic
        image: $IMAGE
        env:
        - name: PCDBHOST
          value: ${NAME_PREFIX}-mysql
        - name: PCDBUSER
          value: root
        - name: PCDBPASS
          valueFrom:
            secretKeyRef:
              name: ${NAME_PREFIX}-secret
              key: password
        ports:
        - containerPort: 8080

---

apiVersion: v1
kind: Service
metadata:
  labels:
    run: ${NAME_PREFIX}-petclinic
  name: ${NAME_PREFIX}-petclinic
  annotations:
    haproxy.org/check: \"enabled\"
    haproxy.org/forwarded-for: \"enabled\"
    haproxy.org/load-balance: \"roundrobin\"
spec:
  selector:
    run: ${NAME_PREFIX}-petclinic
  ports:
  - name: port-1
    port: 80
    protocol: TCP
    targetPort: 8080

---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ${NAME_PREFIX}-petclinic
spec:
  rules:
  - host: $DNS_HOST
    http:
      paths:
      - path: /
        backend:
          serviceName: ${NAME_PREFIX}-petclinic
          servicePort: 80" >deploy.yml

if [[ $# -eq 1 ]]
then
  kubectl apply -f deploy.yml -n $1
else
  kubectl apply -f deploy.yml
fi
