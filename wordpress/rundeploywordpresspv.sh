#!/bin/bash

echo "apiVersion: v1
kind: PersistentVolume
metadata:
  name: ${NAME_PREFIX}-wordpress-pv-volume
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 20Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: \"/mnt/${NAME_PREFIX}-wordpress\"" >deploywordpresspv.yml

if ! kubectl apply -f deploywordpresspv.yml -n $NAME_SPACE
then
  echo "wordpress persistent volume failed to deploy in to $NAME_SPACE"
  exit 1
fi
