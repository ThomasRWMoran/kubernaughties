#!/bin/bash

if kubectl get pv | grep 'wordpress-pv-volume'
then
  if ! ./rundeploywordpresspv.sh
  then
    echo "FULL WORDPRESS DEPLOYMENT failed to deploy in to $NAME_SPACE"
    exit 1
  fi
else
  ./rundeploywordpresspv.sh
fi
