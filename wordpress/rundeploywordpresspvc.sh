#!/bin/bash

echo "apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ${NAME_PREFIX}-wordpress-pv-claim
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20Gi" >deploywordpresspvc.yml

if ! kubectl apply -f deploywordpresspvc.yml -n $NAME_SPACE
then
  echo "wordpress persistent volume claim failed to deploy in to $NAME_SPACE"
  exit 1
fi
