#!/bin/bash

#DNS_HOST=wordpress.prod.kubenaughty.academy.grads.al-labs.co.uk

echo "apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${NAME_PREFIX}-wordpress
  labels:
    app: ${NAME_PREFIX}-wordpress
spec:
  selector:
    matchLabels:
      app: ${NAME_PREFIX}-wordpress
      tier: frontend
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: ${NAME_PREFIX}-wordpress
        tier: frontend
    spec:
      containers:
      - image: docker.io/wordpress:4.8-apache
        name: ${NAME_PREFIX}-wordpress
        env:
        - name: WORDPRESS_DB_HOST
          value: ${NAME_PREFIX}-mysql
        - name: WORDPRESS_DB_NAME
          value: wordpress
        - name: WORDPRESS_DB_USER
          value: root
        - name: WORDPRESS_DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: ${NAME_PREFIX}-mysecret
              key: password
        ports:
        - containerPort: 80
          name: ${NAME_PREFIX}-wordpress
        volumeMounts:
        - name: ${NAME_PREFIX}-wordpress-persistent-storage
          mountPath: /var/www/${NAME_PREFIX}-wordpress/html
      volumes:
      - name: ${NAME_PREFIX}-wordpress-persistent-storage
        persistentVolumeClaim:
          claimName: ${NAME_PREFIX}-wordpress-pv-claim

---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: ${NAME_PREFIX}-wordpress
  name: ${NAME_PREFIX}-wordpress
  annotations:
    haproxy.org/check: \"enabled\"
    haproxy.org/forwarded-for: \"enabled\"
    haproxy.org/load-balance: \"roundrobin\"
spec:
  selector:
    app: ${NAME_PREFIX}-wordpress
  ports:
  - name: port-1
    port: 80
    protocol: TCP
    targetPort: 80

---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ${NAME_PREFIX}-wordpress-ingress
  namespace: ${NAME_SPACE}
spec:
  rules:
  - host: $DNS_HOST
    http:
      paths:
      - path: /
        backend:
          serviceName: ${NAME_PREFIX}-wordpress
          servicePort: 80" >deploywordpress.yml


if [[ $# -eq 1 ]]
then
  if ! kubectl apply -f deploywordpress -n $NAME_SPACE
  then
    echo "wordpress failed to deploy in to $NAME_SPACE"
    exit 1
  fi
else
  kubectl apply -f deploywordpress.yml
fi
