#!/bin/bash

export REG_DNS=$(cat /etc/docker/daemon.json | jq -r '."insecure-registries"[0]')

if curl -s $REG_DNS/v2/mydb/tags/list
  then
  mydb_version=$(curl -sk $REG_DNS/v2/mydb/tags/list | jq '.["tags"][-1]' )
  petclinic_version=$(curl -sk $REG_DNS/v2/petclinic/tags/list | jq '.["tags"][-1]' )

  mydb_version=${mydb_version//\"}
  petclinic_version=${petclinic_version//\"}

  mydb_version=$(echo "$mydb_version + 1" | bc)
  petclinic_version=$(echo "$petclinic_version + 1" | bc)
fi

docker-compose up -d

docker tag mydb:1.0 mydb:$mydb_version
docker rmi mydb:1.0

docker tag petclinic:1.0 petclinic:$mydb_version
docker rmi petclinic:1.0

works=0
time_limit=$((SECONDS+180))

while (( SECONDS < time_limit ))
do
  if curl -s localhost:1080
  then
      echo "Container works"
      works=1
      break
  fi
done

if (( works == 0 ))
then
  echo "Removing Petclinc/Mydb Container & Image"
  docker rm -f petclinic
  docker rm -f mydb
  docker rmi -f mydb:$mydb_version
  docker rmi -f petclinic:$petclinic_version
  docker rmi -f $(docker images | grep none | awk '{print $3}')
else
  echo "Tagging Images"
  docker tag mydb:$mydb_version $REG_DNS/mydb:$mydb_version
  docker tag mydb:$mydb_version $REG_DNS/mydb:latest
  docker tag petclinic:$petclinic_version $REG_DNS/petclinic:$petclinic_version
  docker tag petclinic:$petclinic_version $REG_DNS/petclinic:latest

  docker push $REG_DNS/mydb:$mydb_version
  docker push $REG_DNS/petclinic:$petclinic_version
  docker push $REG_DNS/mydb:latest
  docker push $REG_DNS/petclinic:latest
fi
