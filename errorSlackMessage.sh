#!/bin/bash

generate_post_data()
{
  cat <<EOF
{
  "channel": "#kubernaughty_error_messaging",
  "username": "Error Message",
  "text": "$ERROR_MESSAGE"
}
EOF
}

curl -X POST -H 'Content-type: application/json' --data "$(generate_post_data)" $1
