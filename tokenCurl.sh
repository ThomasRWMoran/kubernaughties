#!/bin/bash
if [[ $# -eq 1 ]]
then
  export dns=$(grep -A1 '^\[jenkins' ${PWD}/jenkinsServer/environments/prod/hosts  | tail -1 | awk '{print $1}')
  curl -vs -H "token: $1" http://admin:feelingnaughty@$dns:8080/generic-webhook-trigger/invoke
else
  echo "Need to give token as argument"
fi
