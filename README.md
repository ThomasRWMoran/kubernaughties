# Docker & Kubernetes Assessment 4

## Description
This repository automates the deployment of Petclinic, Wordpress and Nginx which all connect to a Mysql container inside of a Kubernetes cluster.
This is run as a job on Jenkins which sits outside of the cluster.

Mysql persistent storage within the cluster ensures the database is restored to its last known state on a relaunch of Mysql.
Backups of the database in the form of a mysql dump (stored in a file in an S3 bucket) happen nightly as a Jenkins job so that if the cluster were to go down, a full restoration of the database is still possible.

A combination of AWS, Docker and Kubernetes is used to enable this.

## Documentation
1. Change the variables in the variables.txt file to correspond to yours
2. Run createAll.sh using the following command:
```
./createAll.sh
```
This launches everything outlined in the description above.

For launching applications individually, run tokenCurl.sh followed by the name of the application to be launched.
For example:
```
./tokenCurl.sh mysql
```
The above will launch Mysql and then Petclinic


```
./tokenCurl.sh petclinic
```
The above will launch just Petclinic


```
./tokenCurl.sh wordpress
```
The above will launch just Wordpress


```
./tokenCurl.sh nginx
```
The above will launch just Nginx

## Database Restoration
Prerequisites:

* A running mysql server
* An empty mysql database

Note: This is only if the cluster goes down and automatic restoration via the mysql persistent storage is not possible

Download the mysql dump file and run the following command:
```
mysql -u root -p secret123 <empty database name> < dump.sql
```

## Jenkins Details
* Jenkins URL: http://ec2-54-227-52-148.compute-1.amazonaws.com:8080/
* Username: admin
* Password: feeling naughty

## Mysql Details
* Username: root
* Password: secret123

## Other Useful Links
* Wordpress: http://wordpress.prod.kubenaughty.academy.grads.al-labs.co.uk/
* Petclinic: http://petclinic.prod.kubenaughty.academy.grads.al-labs.co.uk/

```
###--- create_registry ---###
```
Run "./buildReg <AWS PROFILE NAME>" to create EC2 instance, and provision with private docker registry.
SSH key is taken from S3 bucket temporarily, then removed for safety.

```
###--- petclinic ---###
```
Run "./main.sh" to create linked Petclinic application and database container images. Images are curled, to test if they're functioning correctly. If they work, they're saved into the private docker registry both under an updated version number and an overwritten "latest" version for Kubernetes to use.
