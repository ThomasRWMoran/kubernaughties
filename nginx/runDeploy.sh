#!/bin/bash

#GIT_URL=https://ThomasRWMoran@bitbucket.org/ThomasRWMoran/kubenginxhtml.git
#DNS_HOST=web.prod.kubenaughty.academy.grads.al-labs.co.uk

echo "apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: ${NAME_PREFIX}-nginx
  name: ${NAME_PREFIX}-nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      run: ${NAME_PREFIX}-nginx
  template:
    metadata:
      labels:
        run: ${NAME_PREFIX}-nginx
    spec:
      containers:
      - name: ${NAME_PREFIX}-nginx
        image: docker.io/nginx
        ports:
        - containerPort: 80
        volumeMounts:
        - name: ${NAME_PREFIX}-workdir
          mountPath: /usr/share/nginx/html
      initContainers:
      - name: install
        image: alpine/git
        command:
        - git
        - clone
        - $GIT_URL
        - .
        volumeMounts:
        - name: ${NAME_PREFIX}-workdir
          mountPath: \"/git\"
      dnsPolicy: Default
      volumes:
      - name: ${NAME_PREFIX}-workdir
        emptyDir: {}

---

apiVersion: v1
kind: Service
metadata:
  labels:
    run: ${NAME_PREFIX}-nginx
  name: ${NAME_PREFIX}-nginx
  annotations:
    haproxy.org/check: \"enabled\"
    haproxy.org/forwarded-for: \"enabled\"
    haproxy.org/load-balance: \"roundrobin\"
spec:
  selector:
    run: ${NAME_PREFIX}-nginx
  ports:
  - name: port-1
    port: 80
    protocol: TCP
    targetPort: 80

---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ${NAME_PREFIX}-nginx
spec:
  rules:
  - host: $DNS_HOST
    http:
      paths:
      - path: /
        backend:
          serviceName: ${NAME_PREFIX}-nginx
          servicePort: 80" >deploy.yml

if [[ $# -eq 1 ]]
then
  kubectl apply -f deploy.yml -n $1
else
  kubectl apply -f deploy.yml
fi
