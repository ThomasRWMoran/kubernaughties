#!/bin/bash

if kubectl get pv | grep 'mysql-pv-volume'
then
  if ! ./rundeploymysqlpv.sh
  then
    echo "FULL MYSQL DEPLOYMENT failed to deploy in to $NAME_SPACE"
    exit 1
  fi
else
  ./rundeploymysqlpv.sh
fi
