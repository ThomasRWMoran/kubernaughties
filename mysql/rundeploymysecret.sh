#!/bin/bash

echo "apiVersion: v1
data:
  password: c2VjcmV0MTIz
kind: Secret
metadata:
  name: ${NAME_PREFIX}-secret
  namespace: $NAME_SPACE
type: Opaque" >deploymysecret.yml

if ! kubectl apply -f deploymysecret.yml -n $NAME_SPACE
then
  echo "mysecret failed to deploy in to $NAME_SPACE"
  exit 1
fi
