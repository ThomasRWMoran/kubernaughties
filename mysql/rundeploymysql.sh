#!/bin/bash

echo "apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${NAME_PREFIX}-mysql
spec:
  selector:
    matchLabels:
      app: ${NAME_PREFIX}-mysql
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: ${NAME_PREFIX}-mysql
    spec:
      containers:
      - name: ${NAME_PREFIX}-mysql
        image: docker.io/mysql:latest
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: ${NAME_PREFIX}-secret
              key: password
        ports:
        - containerPort: 3306
          name: ${NAME_PREFIX}-mysql
        volumeMounts:
        - name: ${NAME_PREFIX}-mysql-persistent-storage
          mountPath: /var/lib/mysql
      volumes:
      - name: ${NAME_PREFIX}-mysql-persistent-storage
        persistentVolumeClaim:
          claimName: ${NAME_PREFIX}-mysql-pv-claim
---

apiVersion: v1
kind: Service
metadata:
  name: ${NAME_PREFIX}-mysql
spec:
  ports:
  - port: 3306
  selector:
    app: ${NAME_PREFIX}-mysql
  clusterIP: None" >deploymysql.yml

if ! kubectl apply -f deploymysql.yml -n $NAME_SPACE
then
  echo "mysql failed to deploy in to $NAME_SPACE"
  exit 1
fi
