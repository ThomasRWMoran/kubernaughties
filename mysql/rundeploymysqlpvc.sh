#!/bin/bash

echo "apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ${NAME_PREFIX}-mysql-pv-claim
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20Gi" >deploymysqlpvc.yml

 if ! kubectl apply -f deploymysqlpvc.yml -n $NAME_SPACE
 then
   echo "mysql persistent volume claim failed to deploy in to $NAME_SPACE"
   exit 1
 fi
