#!/bin/bash

echo "apiVersion: v1
kind: PersistentVolume
metadata:
  name: ${NAME_PREFIX}-mysql-pv-volume
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 20Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: \"/mnt/${NAME_PREFIX}mysql\"" >deploymysqlpv.yml

if ! kubectl apply -f deploymysqlpv.yml -n $NAME_SPACE
then
  echo "mysql persistent volume failed to deploy in to $NAME_SPACE"
  exit 1
fi
