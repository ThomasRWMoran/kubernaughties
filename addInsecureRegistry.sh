#!/bin/bash
ANSIBLEENV=prod
export REG_DNS=$(grep -A1 '^\[registry' ${PWD}/create_registry/environments/${ANSIBLEENV}/hosts  | tail -1 | awk '{print $1}')
export JENKINS_DNS=$(grep -A1 '^\[jenkins' ${PWD}/jenkinsServer/environments/${ANSIBLEENV}/hosts  | tail -1 | awk '{print $1}')
export KUBEMASTER_IP=$(grep -A1 '^\[kubemaster' ${PWD}/K8sCluster/environments/${ANSIBLEENV}/hosts  | tail -1 | awk '{print $1}')
export KUBEWORKER_1_IP=$(grep -A1 '^\[kubeworker' ${PWD}/K8sCluster/environments/${ANSIBLEENV}/hosts  | tail -1 | awk '{print $1}')
export KUBEWORKER_2_IP=$(grep -A2 '^\[kubeworker' ${PWD}/K8sCluster/environments/${ANSIBLEENV}/hosts  | tail -1 | awk '{print $1}')

aws s3 cp s3://${S3_BUCKET_NAME}/${S3_KEY_FILE} /tmp/${S3_KEY_FILE}
chmod 600 /tmp/${S3_KEY_FILE}

echo "{
  \"insecure-registries\" : [\"${REG_DNS}:5000\"]
}" >/tmp/daemon.json

for dns in $JENKINS_DNS $KUBEMASTER_IP $KUBEWORKER_1_IP $KUBEWORKER_2_IP
do
  scp -i /tmp/${S3_KEY_FILE} /tmp/daemon.json -o "StrictHostKeyChecking no" ec2-user@${dns}:/home/ec2-user/daemon.json
  ssh -i /tmp/${S3_KEY_FILE} -o "StrictHostKeyChecking no" ec2-user@${dns} "sudo cp /home/ec2-user/daemon.json /etc/docker/daemon.json"
  ssh -i /tmp/${S3_KEY_FILE} -o "StrictHostKeyChecking no" ec2-user@${dns} "sudo systemctl restart docker"
done

rm /tmp/${S3_KEY_FILE} /tmp/daemon.json
