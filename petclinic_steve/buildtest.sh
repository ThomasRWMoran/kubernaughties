#!/bin/bash

# Script to build and test PetClinic docker images
dbok=0
pcok=0
VERSION=1.0

# Start the DB instance
if ! docker build -t pcdb:$VERSION -f Dockerfile.buildDB .
then
	dbok=1
	#exit 1
fi

# Build petclinic
if (( dbok == 0 ))
then
	if ! docker build -t pc:$VERSION -f Dockerfile.buildPC .
	then
		docker rmi pcdb:$VERSION
		pcok=1
		#exit 1
	fi
fi

# Test PetClinic
if (( dbok == 0 )) && (( pcok == 0 ))
then
	docker run -itd --name pcdb -e MYSQL_ROOT_PASSWORD=secret123 pcdb:$VERSION
	docker run -itd --name petclinic -P --link pcdb:pcdb -e PCDBHOST=pcdb -e PCDBUSER=root -e PCDBPASS=secret123 pc:$VERSION
fi

# Clean up none images
docker rmi $(docker images | grep none | awk '{print $3}')
#docker rm -f pcdb petclinic
