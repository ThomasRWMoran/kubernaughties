#!/bin/bash
export dns=$(grep -A1 '^\[jenkins' ${PWD}/jenkinsServer/environments/${ANSIBLEENV}/hosts  | tail -1 | awk '{print $1}')
curl -vs -H "token: start" http://admin:feeling\ naughty@$dns:8080/generic-webhook-trigger/invoke
