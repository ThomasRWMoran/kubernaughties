#!/bin/bash
source variables.txt
export AWS_DEFAULT_PROFILE PYTHON_INTERPRETER AWS_DEFAULT_REGION AWS_CREDENTIALS_PATH AWS_SSH_KEY ANSIBLEENV S3_KEY_FILE S3_BUCKET_NAME PROJECT_NAME AWS_ENVIRONMENT REG_EC2_TYPE EC2_LINUX_AMI_ID PUBLIC_SUBNET_ID SECURE_SECGRP_NAME JENKINS_EC2_TYPE S3_IAM_ROLE SECURE_SECGRP_ID PROJECT_OWNER PROJECT_START_DATE PROJECT_END_DATE VPC_ID LB_SUBNET_ID DNS_ZONE DNS_ZONE_ID
export ANSIBLEENV=prod

cd K8sCluster
./createFtKey

export KUBEMASTER_IP=$(grep -A1 '^\[kubemaster' ${PWD}/environments/${ANSIBLEENV}/hosts  | tail -1 | awk '{print $1}')

cd ../jenkinsServer
./buildec2 $AWS_DEFAULT_PROFILE createJenkins.yml

cd ../create_registry
./buildReg

cd ..
./addInsecureRegistry.sh
sleep 2m
./startCurl.sh
sleep 30m
./tokenCurl.sh wordpress
sleep 10m
./tokenCurl.sh nginx
